<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Exceptions;

use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Throwable;
use Xint0\CredentialStorage\Contracts\CredentialFactoryExceptionInterface;
use Xint0\CredentialStorage\Contracts\CredentialStoreExceptionInterface;

/**
 * Exception that is thrown when the API service encounters and error condition.
 */
class ApiException extends RuntimeException
{
    public const CODE_CREDENTIAL_CREATION_FAILED = 4;
    public const CODE_CREDENTIAL_STORAGE_FAILED = 5;
    public const CODE_HTTP_CLIENT_INITIALIZATION_FAILED = 1;
    public const CODE_HTTP_CLIENT_SEND_REQUEST_FAILED = 3;
    public const CODE_MAKE_REQUEST_FAILED = 2;

    public static function credentialCreationFailed(CredentialFactoryExceptionInterface $previous): self
    {
        return new ApiException(
            'Credential creation failed.',
            self::CODE_CREDENTIAL_CREATION_FAILED,
            $previous
        );
    }

    public static function credentialStorageFailed(CredentialStoreExceptionInterface $previous): self
    {
        return new ApiException(
            'Credential storage failed.',
            self::CODE_CREDENTIAL_STORAGE_FAILED,
            $previous
        );
    }

    public static function httpClientInitializationFailed(?Throwable $previous = null): self
    {
        return new ApiException(
            'HTTP client initialization failed.',
            self::CODE_HTTP_CLIENT_INITIALIZATION_FAILED,
            $previous
        );
    }

    public static function httpClientSendRequestFailed(ClientExceptionInterface $previous): self
    {
        return new ApiException(
            'HTTP request failed.',
            self::CODE_HTTP_CLIENT_SEND_REQUEST_FAILED,
            $previous
        );
    }

    public static function makeRequestFailed(string $request_type, RequestFactoryException $previous): self
    {
        return new ApiException(
            sprintf('Could not make %s request.', $request_type),
            self::CODE_MAKE_REQUEST_FAILED,
            $previous,
        );
    }

    public static function httpRequestFailed(int $http_status_code): self
    {
        return new ApiException(
            sprintf('Request failed with HTTP status code %d.', $http_status_code),
            $http_status_code
        );
    }
}
