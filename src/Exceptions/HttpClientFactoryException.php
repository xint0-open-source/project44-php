<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Exceptions;

use Http\Discovery\Exception\NotFoundException;
use RuntimeException;

/**
 * Exception that is thrown when the HTTP client factory encounters an error.
 */
class HttpClientFactoryException extends RuntimeException
{
    /**
     * Returns an exception to be thrown when factory does not find an HTTP client implementation.
     *
     * @param  NotFoundException  $exception
     *
     * @return self
     */
    public static function fromNotFoundException(NotFoundException $exception): self
    {
        return new self("HTTP client implementation not found.", 1, $exception);
    }
}
