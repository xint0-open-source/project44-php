<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual <rogelio.jacinto@gmail.com>
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/credential-storage-contract/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Exceptions;

use Http\Discovery\Exception\NotFoundException;
use RuntimeException;
use Xint0\CredentialStorage\Contracts\CredentialStoreExceptionInterface;

class RequestFactoryException extends RuntimeException
{
    private const ERROR_CODE_COULD_NOT_RETRIEVE_CLIENT_CREDENTIAL = 1;
    private const ERROR_CODE_REQUEST_FACTORY_NOT_FOUND = 2;
    private const ERROR_CODE_STREAM_FACTORY_NOT_FOUND = 4;
    private const ERROR_CODE_URI_FACTORY_NOT_FOUND = 3;
    private const ERROR_MESSAGE_COULD_NOT_RETRIEVE_CLIENT_CREDENTIAL = "Could not retrieve client credential '%s'";
    private const ERROR_MESSAGE_REQUEST_FACTORY_NOT_FOUND = 'Request factory not found';
    private const ERROR_MESSAGE_STREAM_FACTORY_NOT_FOUND = 'Stream factory not found';
    private const ERROR_MESSAGE_URI_FACTORY_NOT_FOUND = 'URI factory not found';

    public static function couldNotRetrieveClientCredential(
        string $clientId,
        CredentialStoreExceptionInterface $previous
    ): self {
        return new self(
            sprintf(self::ERROR_MESSAGE_COULD_NOT_RETRIEVE_CLIENT_CREDENTIAL, $clientId),
            self::ERROR_CODE_COULD_NOT_RETRIEVE_CLIENT_CREDENTIAL,
            $previous
        );
    }

    public static function requestFactoryNotFound(NotFoundException $previous): self
    {
        return new self(
            self::ERROR_MESSAGE_REQUEST_FACTORY_NOT_FOUND,
            self::ERROR_CODE_REQUEST_FACTORY_NOT_FOUND,
            $previous
        );
    }

    public static function uriFactoryNotFound(NotFoundException $previous): self
    {
        return new self(
            self::ERROR_MESSAGE_URI_FACTORY_NOT_FOUND,
            self::ERROR_CODE_URI_FACTORY_NOT_FOUND,
            $previous
        );
    }

    public static function streamFactoryNotFound(NotFoundException $previous): self
    {
        return new self(
            self::ERROR_MESSAGE_STREAM_FACTORY_NOT_FOUND,
            self::ERROR_CODE_STREAM_FACTORY_NOT_FOUND,
            $previous
        );
    }
}
