<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Enums;

/**
 * Shipment identifier type
 */
enum ShipmentIdentifierType: string
{
    case BILL_OF_LADING = 'BILL_OF_LADING';
    case ORDER = 'ORDER';
}
