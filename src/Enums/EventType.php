<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Enums;

/**
 * Event type
 */
enum EventType: string
{
    case ARRIVED = 'ARRIVED';
    case CANCELLED = 'CANCELLED';
    case DELIVERED = 'DELIVERED';
    case DEPARTED = 'DEPARTED';
    case DISPATCHED = 'DISPATCHED';
    case LOADING = 'LOADING';
    case POSITION = 'POSITION';
    case SCANNED_AT_STOP = 'SCANNED_AT_STOP';
    case UNLOADING = 'UNLOADING';
}
