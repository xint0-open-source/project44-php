<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\Project44\Exceptions\ApiException;
use Xint0\Project44\Exceptions\HttpClientFactoryException;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Factories\HttpClientFactory;
use Xint0\Project44\Factories\RequestFactory;
use Xint0\Project44\Models\StatusUpdate;

/**
 * Project44 API service.
 */
class Api implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private ClientInterface $httpClient;
    private RequestFactory $requestFactory;

    public function __construct(
        CredentialStoreInterface $credentialStore,
        ?ClientInterface $client = null,
        ?LoggerInterface $logger = null
    ) {
        $this->initializeLogger($logger);
        $this->initializeClient($client);
        $this->initializeRequestFactory($credentialStore);
    }

    /**
     * Requests creation of a new access token.
     *
     * @return ResponseInterface
     *
     * @throws ApiException When request cannot be created.
     * @throws ApiException When request cannot be sent.
     * @throws ApiException When response cannot be read.
     * @throws ApiException When response HTTP status is not successful.
     */
    public function createAccessToken(): ResponseInterface
    {
        $response = $this->sendRequest($this->makeCreateAccessTokenRequest());
        $this->validateResponseStatus($response);
        return $response;
    }

    /**
     * Requests creation of shipment status update.
     *
     * @param  StatusUpdate  $statusUpdate
     *
     * @return ResponseInterface
     *
     * @throws ApiException When request cannot be created.
     * @throws ApiException When request cannot be sent.
     * @throws ApiException When response cannot be read.
     * @throws ApiException When response HTTP status is not successful.
     */
    public function createStatusUpdate(StatusUpdate $statusUpdate): ResponseInterface
    {
        $response = $this->sendRequest($this->makeCreateStatusUpdateRequest($statusUpdate));
        $this->validateResponseStatus($response);
        return $response;
    }

    public function httpClient(): ClientInterface
    {
        return $this->httpClient;
    }

    public function setHttpClient(ClientInterface $client): void
    {
        $this->httpClient = $client;
    }

    private function initializeLogger(?LoggerInterface $logger = null): void
    {
        $this->setLogger($logger ?? new NullLogger());
        $this->logger?->debug(__METHOD__ . ' - logger initialized.');
    }

    private function initializeClient(?ClientInterface $client = null): void
    {
        if ($client === null) {
            $clientFactory = new HttpClientFactory();
            try {
                $client = $clientFactory->make();
            } catch (HttpClientFactoryException $exception) {
                $this->logger?->error(__METHOD__ . ' - could not create HTTP client.');
                throw ApiException::httpClientInitializationFailed($exception);
            }
            $this->logger?->debug(__METHOD__ . ' - created default HTTP client.');
        }

        $this->setHttpClient($client);
        $this->logger?->debug(__METHOD__ . ' - initialized HTTP client.');
    }

    private function initializeRequestFactory(CredentialStoreInterface $credentialStore): void
    {
        $this->requestFactory = new RequestFactory($credentialStore);
    }

    /**
     * @return RequestInterface
     *
     * @throws ApiException When request factory fails to make request.
     */
    private function makeCreateAccessTokenRequest(): RequestInterface
    {
        try {
            return $this->requestFactory->makeCreateAccessTokenRequest();
        } catch (RequestFactoryException $exception) {
            $this->logger?->error(
                __METHOD__ . ' - create access token request creation failed.',
                compact('exception')
            );
            throw ApiException::makeRequestFailed('create access token', $exception);
        }
    }

    private function makeCreateStatusUpdateRequest(StatusUpdate $statusUpdate): RequestInterface
    {
        try {
            return $this->requestFactory->makeCreateStatusUpdateRequest($statusUpdate);
        } catch (RequestFactoryException $exception) {
            $this->logger?->error(
                __METHOD__ . ' - create status update request creation failed.',
                compact('exception')
            );
            throw ApiException::makeRequestFailed('create status update', $exception);
        }
    }

    /**
     * @param  RequestInterface  $request
     *
     * @return ResponseInterface
     *
     * @throws ApiException When request send fails.
     */
    private function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            return $this->httpClient()->sendRequest($request);
        } catch (ClientExceptionInterface $exception) {
            $this->logger?->error(__METHOD__ . ' - Failed to send request.', compact('exception', 'request'));
            throw ApiException::httpClientSendRequestFailed($exception);
        }
    }

    /**
     * @param  ResponseInterface  $response
     *
     * @return ResponseInterface
     *
     * @throws ApiException When response HTTP status code is not successful.
     */
    private function validateResponseStatus(ResponseInterface $response): ResponseInterface
    {
        $http_status_code = $response->getStatusCode();
        if ($http_status_code === 200 || $http_status_code === 202) {
            return $response;
        }

        $body = $response->getBody()->getContents();

        $this->logger?->error(__METHOD__ . ' - response status is unsuccessful.', compact('http_status_code', 'body'));
        throw ApiException::httpRequestFailed($http_status_code);
    }
}
