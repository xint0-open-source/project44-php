<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Models;

use JsonSerializable;
use Xint0\Project44\Enums\ShipmentIdentifierType;

class ShipmentIdentifier implements JsonSerializable
{
    private ShipmentIdentifierType $type;
    private string $value;

    /**
     * Creates a new shipment identifier instance.
     *
     * @param  ShipmentIdentifierType  $type
     * @param  string  $value
     */
    public function __construct(ShipmentIdentifierType $type, string $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * Returns the type of identifier.
     *
     * @return ShipmentIdentifierType
     */
    public function type(): ShipmentIdentifierType
    {
        return $this->type;
    }

    /**
     * Returns the value of the identifier.
     *
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return array{type:string,value:string}
     */
    public function jsonSerialize(): array
    {
        return [
            'type' => $this->type->value,
            'value' => $this->value,
        ];
    }
}
