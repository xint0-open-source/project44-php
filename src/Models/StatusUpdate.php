<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Models;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use JsonSerializable;
use Xint0\Project44\Enums\EventType;

class StatusUpdate implements JsonSerializable
{
    private string $customerId;
    private int $eventStopNumber;
    private EventType $eventType;
    private float $latitude;
    private float $longitude;
    /** @var ShipmentIdentifier[] */
    private array $shipmentIdentifiers;
    private DateTimeImmutable $utcTimestamp;

    public function __construct(string $customerId, ?DateTimeInterface $timestamp = null)
    {
        $this->customerId = $customerId;
        $this->eventStopNumber = 0;
        $this->eventType = EventType::POSITION;
        $this->shipmentIdentifiers = [];
        $this->latitude = 0;
        $this->longitude = 0;
        $this->initTimestamp($timestamp);
    }

    public function addShipmentIdentifier(ShipmentIdentifier $shipmentIdentifier): self
    {
        $this->shipmentIdentifiers[] = $shipmentIdentifier;
        return $this;
    }

    public function customerId(): string
    {
        return $this->customerId;
    }

    public function eventStopNumber(): int
    {
        return $this->eventStopNumber;
    }

    public function eventType(): EventType
    {
        return $this->eventType;
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }

    public function setEventStopNumber(int $eventStopNumber): self
    {
        $this->eventStopNumber = $eventStopNumber;
        return $this;
    }

    public function setEventType(EventType $eventType): self
    {
        $this->eventType = $eventType;
        return $this;
    }

    public function setPosition(float $longitude, float $latitude): self
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return array|ShipmentIdentifier[]
     */
    public function shipmentIdentifiers(): array
    {
        return $this->shipmentIdentifiers;
    }

    /**
     * @return array{
     *     customerId: string,
     *     eventStopNumber?: int,
     *     eventType: string,
     *     shipmentIdentifiers: array<int,array{type:string,value:string}>,
     *     latitude: float,
     *     longitude: float,
     *     utcTimestamp: string,
     * }
     */
    public function jsonSerialize(): array
    {
        $result = [
            'customerId' => $this->customerId,
            'eventType' => $this->eventType->value,
            'eventStopNumber' => $this->eventStopNumber,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'shipmentIdentifiers' => array_map(
                fn (ShipmentIdentifier $i) => $i->jsonSerialize(),
                $this->shipmentIdentifiers
            ),
            'utcTimestamp' => $this->utcTimestamp->format('Y-m-d\\TH:i:s'),
        ];
        if ($this->eventStopNumber === 0) {
            unset($result['eventStopNumber']);
        }

        return $result;
    }

    public function utcTimestamp(): DateTimeImmutable
    {
        return $this->utcTimestamp;
    }

    private function initTimestamp(?DateTimeInterface $dateTime): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $timestamp = $dateTime === null ?
            new DateTimeImmutable('now', new DateTimeZone('UTC')) :
            DateTimeImmutable::createFromInterface($dateTime);
        $this->utcTimestamp = $timestamp->setTimezone(new DateTimeZone('UTC'));
    }
}
