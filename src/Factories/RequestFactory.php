<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Factories;

use Http\Discovery\Exception\NotFoundException;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Xint0\CredentialStorage\Contracts\CredentialStoreExceptionInterface;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Models\StatusUpdate;

class RequestFactory
{
    private CredentialStoreInterface $credentialStore;
    private RequestFactoryInterface $factory;
    private StreamFactoryInterface $streamFactory;
    private UriFactoryInterface $uriFactory;

    /**
     * Creates a new instance of the RequestFactory class.
     *
     * @throws RequestFactoryException When PSR-17 request factory is not found.
     * @throws RequestFactoryException When PSR-17 stream factory is not found.
     * @throws RequestFactoryException When PSR-17 uri factory is not found.
     */
    public function __construct(CredentialStoreInterface $credentialStore)
    {
        $this->initCredentialStore($credentialStore);
        $this->initUriFactoryInterface();
        $this->initRequestFactoryInterface();
        $this->initStreamFactoryInterface();
    }

    public function credentialStore(): CredentialStoreInterface
    {
        return $this->credentialStore;
    }

    /**
     * @return RequestInterface
     *
     * @throws RequestFactoryException When client credential cannot be retrieved from credential store.
     */
    public function makeCreateAccessTokenRequest(): RequestInterface
    {
        $uri = $this->uriFactory->createUri()->withScheme('https')
            ->withHost('na12.api.project44.com')
            ->withPath('/api/v4/oauth2/token');
        $bodyStream = $this->streamFactory->createStream('grant_type=' . urlencode('client_credentials'));
        return $this->factory->createRequest('POST', $uri)
            ->withHeader('Authorization', $this->basicAuthorizationHeader())
            ->withHeader('Content-Type', 'application/x-www-form-urlencoded')
            ->withHeader('Accept', 'application/json')
            ->withBody($bodyStream);
    }

    public function makeCreateStatusUpdateRequest(StatusUpdate $statusUpdate): RequestInterface
    {
        $uri = $this->uriFactory->createUri()->withScheme('https')
            ->withHost('na12.api.project44.com')
            ->withPath('/api/v4/capacityproviders/tl/shipments/statusUpdates');
        $bodyStream = $this->streamFactory->createStream(json_encode($statusUpdate, JSON_THROW_ON_ERROR));
        return $this->factory->createRequest('POST', $uri)
            ->withHeader('Authorization', $this->bearerAuthorizationHeader())
            ->withHeader('Content-Type', 'application/json')
            ->withHeader('Accept', 'application/json')
            ->withBody($bodyStream);
    }

    private function initCredentialStore(CredentialStoreInterface $credentialStore): void
    {
        $this->credentialStore = $credentialStore;
    }

    /**
     * @throws RequestFactoryException When PSR-17 Request factory is not found.
     */
    private function initRequestFactoryInterface(): void
    {
        try {
            $this->factory = Psr17FactoryDiscovery::findRequestFactory();
        } catch (NotFoundException $exception) {
            throw RequestFactoryException::requestFactoryNotFound($exception);
        }
    }

    /** @throws RequestFactoryException When PSR-17 URI factory is not found. */
    private function initUriFactoryInterface(): void
    {
        try {
            $this->uriFactory = Psr17FactoryDiscovery::findUriFactory();
        } catch (NotFoundException $exception) {
            throw RequestFactoryException::uriFactoryNotFound($exception);
        }
    }

    /** @throws RequestFactoryException When PSR-17 Stream factory is not found. */
    private function initStreamFactoryInterface(): void
    {
        try {
            $this->streamFactory = Psr17FactoryDiscovery::findStreamFactory();
        } catch (NotFoundException $exception) {
            throw RequestFactoryException::streamFactoryNotFound($exception);
        }
    }

    /**
     * @throws RequestFactoryException When client credential cannot be retrieved from credential store.
     */
    private function basicAuthorizationHeader(): string
    {
        try {
            $clientCredential = $this->credentialStore->getCredential('project44_client');
        } catch (CredentialStoreExceptionInterface $exception) {
            throw RequestFactoryException::couldNotRetrieveClientCredential('project44_client', $exception);
        }
        return 'Basic ' . base64_encode($clientCredential->getIdentity() . ':' . $clientCredential->decryptSecret());
    }

    /**
     * @return string
     *
     * @throws RequestFactoryException When token credential cannot be retrieved from credential store.
     */
    private function bearerAuthorizationHeader(): string
    {
        try {
            $tokenCredential = $this->credentialStore->getCredential('project44_token');
        } catch (CredentialStoreExceptionInterface $exception) {
            throw RequestFactoryException::couldNotRetrieveClientCredential('project44_token', $exception);
        }
        return 'Bearer ' . $tokenCredential->decryptSecret();
    }
}
