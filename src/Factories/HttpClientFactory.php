<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\Project44\Factories;

use Http\Discovery\Exception\NotFoundException;
use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientInterface;
use Xint0\Project44\Exceptions\HttpClientFactoryException;

class HttpClientFactory
{
    /**
     * Returns an HTTP client.
     *
     * @return ClientInterface
     *
     * @throws HttpClientFactoryException When HTTP client factory could not find a client implementation.
     */
    public function make(): ClientInterface
    {
        try {
            return Psr18ClientDiscovery::find();
        } catch (NotFoundException $exception) {
            throw HttpClientFactoryException::fromNotFoundException($exception);
        }
    }
}
