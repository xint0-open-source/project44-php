# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.3.0...master)

### Changed

- Set phpstan level to 9.
- `ShipmentIdentifier::jsonSerialize` serialize enum as string value.
- `StatusUpdate::jsonSerialize` explicit array serialization.
- Use null coalescing operator in case `Api::$logger` is null.

## [0.3.0](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.2.1...0.3.0) 2024-09-02

### Changed

- Upgrade `xint0/credential-storage-contract` to `^1.0`.
- Upgrade `xint0/mock-credential-store` to `^1.0`.

## [0.2.1](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.2.0...0.2.1) 2024-06-27

### Changed

- Drop support for PHP 8.0.
- Add PHP 8.3 to CI pipeline.
- Upgrade PHPUnit to 10.5.
- Convert `EventType` and `ShipmentIdentifierType` to native enums.
- Remove `myclabs/php-enum` dependency.
- Support `psr/http-message` ^1.0 || ^2.0.
- Support `psr/log` ^2.0 || ^3.0.
- Use `Http\Discovery\Psr18ClientDiscovery` instead of `Http\Discovery\HttpClientDiscovery`.
- Use `xint0/mock-credential-store` for testing.

## [0.2.0](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.1.1...0.2.0) 2023-11-15

### Changed

- Upgrade `psr/log` to version 2.0.

## [0.1.1](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.1.0...0.1.1) 2023-08-03

### Changed

- Replace `php-http/message-factory` with `psr/http-factory-implementation`.
- Replace deprecated `Http\Discovery\HttpClientDiscovery` with `Http\Discovery\Psr18ClientDiscovery`.
- Remove direct dependency on `php-http/httplug`.

## [0.1.0](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.0.6...0.1.0) 2023-08-03

### Changed

- Drop support for PHP 7.4.

## [0.0.6](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.0.5...0.0.6) 2023-02-14

### Changed

- Change `utcTimestamp` format to not include UTC offset.

## [0.0.5](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.0.4...0.0.5) 2023-02-14

### Changed

- Log response body when HTTP status code is not successful.

## [0.0.4](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.0.3...0.0.4) 2023-02-07

### Changed

- Simplified Api@createAccessToken, do not require CredentialFactory and return ResponseInterface on success.
- Api@createStatusUpdate returns ResponseInterface on success.
- Api implements LoggerAwareInterface to set logger for logging notable events.

### Added

- Log Api errors using LoggerInterface.

## [0.0.3](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.0.2...0.0.3) 2023-02-03

### Changed

- Ignore docker directory, .gitattributes, and .gitignore files on git export.

## [0.0.2](https://gitlab.com/xint0-open-source/project44-php/-/compare/0.0.1...0.0.2) 2023-02-03

### Changed

- Add PHPDoc annotations for static methods of `EventType` and `ShipmentIdentifierType` enums.

## [0.0.1](https://gitlab.com/xint0-open-source/project44-php/-/tags/0.0.1) 2023-02-03

### Added

- `Api@createAccessToken` create access token for authenticating requests.
- `Api@createStatusUpdate` create shipment status update.