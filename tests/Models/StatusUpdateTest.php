<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Models;

use DateTimeImmutable;
use DateTimeZone;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\TestCase;
use Xint0\Project44\Enums\EventType;
use Xint0\Project44\Enums\ShipmentIdentifierType;
use Xint0\Project44\Models\ShipmentIdentifier;
use Xint0\Project44\Models\StatusUpdate;

#[CoversClass(StatusUpdate::class)]
#[UsesClass(ShipmentIdentifier::class)]
class StatusUpdateTest extends TestCase
{
    public function test_new_instance_has_expected_default_properties(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $expected_time = new DateTimeImmutable('now', new DateTimeZone('UTC'));
        $expected_time_string = $expected_time->format(DATE_ATOM);
        $sut = new StatusUpdate('customerId');
        $this->assertSame('customerId', $sut->customerId());
        $this->assertSame([], $sut->shipmentIdentifiers());
        $this->assertSame(0, $sut->eventStopNumber());
        $this->assertSame(EventType::POSITION, $sut->eventType());
        $this->assertSame(0.0, $sut->latitude());
        $this->assertSame(0.0, $sut->longitude());
        $this->assertSame($expected_time_string, $sut->utcTimestamp()->format(DATE_ATOM));
    }

    public function test_json_encode_instance_with_default_values_returns_expected_value(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $expectedTimestamp = (new DateTimeImmutable('now', new DateTimeZone('UTC')))->format('Y-m-d\\TH:i:s');
        $expected = '{"customerId":"CustomerId","eventType":"POSITION","latitude":0,"longitude":0,"shipmentIdentifiers":[],"utcTimestamp":"' . $expectedTimestamp . '"}';
        $sut = new StatusUpdate('CustomerId');
        $this->assertSame($expected, json_encode($sut));
    }

    public function test_constructor_accepts_optional_timestamp_and_converts_it_to_utc(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $timestamp = new DateTimeImmutable('2023-02-02 10:28:12', new DateTimeZone('America/Denver'));
        $expectedTimestampString = '2023-02-02T17:28:12+00:00';
        $sut = new StatusUpdate('CustomerId', $timestamp);
        $this->assertSame($expectedTimestampString, $sut->utcTimestamp()->format(DATE_ATOM));
    }

    public function test_set_event_type_updates_the_event_type(): void
    {
        $newEventType = EventType::ARRIVED;
        $sut = new StatusUpdate('CustomerId');

        $sut->setEventType($newEventType);

        $this->assertSame($newEventType, $sut->eventType());
    }

    public function test_add_shipment_identifier_updates_shipment_identifier_list(): void
    {
        $newShipmentIdentifier = new ShipmentIdentifier(ShipmentIdentifierType::BILL_OF_LADING, 'bill of lading');

        $sut = new StatusUpdate('CustomerId');

        $sut->addShipmentIdentifier($newShipmentIdentifier);

        $this->assertSame([$newShipmentIdentifier], $sut->shipmentIdentifiers());
    }

    public function test_can_add_multiple_shipment_identifiers(): void
    {
        $firstShipmentIdentifier = new ShipmentIdentifier(ShipmentIdentifierType::ORDER, 'order number');
        $secondShipmentIdentifier = new ShipmentIdentifier(ShipmentIdentifierType::BILL_OF_LADING, 'bill of lading number');

        $sut = new StatusUpdate('CustomerId');

        $sut->addShipmentIdentifier($firstShipmentIdentifier)
            ->addShipmentIdentifier($secondShipmentIdentifier);

        $this->assertSame([$firstShipmentIdentifier, $secondShipmentIdentifier], $sut->shipmentIdentifiers());
    }

    public function test_set_position_updates_longitude_and_latitude(): void
    {
        $longitude = -103.4568;
        $latitude = 30.12345;

        $sut = new StatusUpdate('CustomerId');

        $sut->setPosition($longitude, $latitude);

        $this->assertSame($longitude, $sut->longitude());
        $this->assertSame($latitude, $sut->latitude());
    }

    public function test_set_event_stop_number_updates_event_stop_number(): void
    {
        $newEventStopNumber = 4;

        $sut = new StatusUpdate('CustomerId');

        $sut->setEventStopNumber($newEventStopNumber);

        $this->assertSame($newEventStopNumber, $sut->eventStopNumber());
    }
}
