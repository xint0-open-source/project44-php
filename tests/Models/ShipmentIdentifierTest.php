<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Models;

use PHPUnit\Framework\Attributes\CoversClass;
use Tests\TestCase;
use Xint0\Project44\Enums\ShipmentIdentifierType;
use Xint0\Project44\Models\ShipmentIdentifier;

#[CoversClass(ShipmentIdentifier::class)]
class ShipmentIdentifierTest extends TestCase
{
    public function test_type_method_returns_expected_value(): void
    {
        $sut = new ShipmentIdentifier(ShipmentIdentifierType::BILL_OF_LADING, 'bill of lading');
        $this->assertSame(ShipmentIdentifierType::BILL_OF_LADING, $sut->type());
    }

    public function test_value_method_returns_expected_value(): void
    {
        $sut = new ShipmentIdentifier(ShipmentIdentifierType::BILL_OF_LADING, 'bill of lading');
        $this->assertSame('bill of lading', $sut->value());
    }

    public function test_implements_json_serializable_interface(): void
    {
        $sut = new ShipmentIdentifier(ShipmentIdentifierType::ORDER, 'order number');
        $this->assertSame(['type' => ShipmentIdentifierType::ORDER->value, 'value' => 'order number'], $sut->jsonSerialize());
    }

    public function test_json_encode_instance_returns_expected_value(): void
    {
        $sut = new ShipmentIdentifier(ShipmentIdentifierType::BILL_OF_LADING, 'bill of lading');
        $this->assertSame('{"type":"BILL_OF_LADING","value":"bill of lading"}', json_encode($sut));
    }
}
