<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Exceptions;

use Http\Discovery\Exception\NotFoundException;
use PHPUnit\Framework\Attributes\CoversClass;
use Tests\Fakes\CredentialStoreExceptionFake;
use Xint0\Project44\Exceptions\RequestFactoryException;
use PHPUnit\Framework\TestCase;

#[CoversClass(RequestFactoryException::class)]
class RequestFactoryExceptionTest extends TestCase
{
    public function test_could_not_retrieve_client_credential_returns_instance_with_expected_properties(): void
    {
        $previous = new CredentialStoreExceptionFake('Credential not found');
        $actual = RequestFactoryException::couldNotRetrieveClientCredential('project44_client', $previous);
        $this->assertSame('Could not retrieve client credential \'project44_client\'', $actual->getMessage());
        $this->assertSame(1, $actual->getCode());
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_request_factory_not_found_returns_instance_with_expected_properties(): void
    {
        $previous = new NotFoundException();
        $actual = RequestFactoryException::requestFactoryNotFound($previous);
        $this->assertSame('Request factory not found', $actual->getMessage());
        $this->assertSame(2, $actual->getCode());
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_uri_factory_not_found_returns_instance_with_expected_properties(): void
    {
        $previous = new NotFoundException();
        $actual = RequestFactoryException::uriFactoryNotFound($previous);
        $this->assertSame('URI factory not found', $actual->getMessage());
        $this->assertSame(3, $actual->getCode());
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_stream_factory_not_found_returns_instance_with_expected_properties(): void
    {
        $previous = new NotFoundException();
        $actual = RequestFactoryException::streamFactoryNotFound($previous);
        $this->assertSame('Stream factory not found', $actual->getMessage());
        $this->assertSame(4, $actual->getCode());
        $this->assertSame($previous, $actual->getPrevious());
    }
}
