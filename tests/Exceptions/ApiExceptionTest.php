<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Exceptions;

use Http\Client\Exception\NetworkException;
use Http\Discovery\Exception\NotFoundException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Tests\Fakes\CredentialFactoryExceptionFake;
use Tests\Fakes\CredentialStoreExceptionFake;
use Tests\TestCase;
use Xint0\Project44\Exceptions\ApiException;
use Xint0\Project44\Exceptions\HttpClientFactoryException;
use Xint0\Project44\Exceptions\RequestFactoryException;

#[CoversClass(ApiException::class)]
#[UsesClass(HttpClientFactoryException::class)]
class ApiExceptionTest extends TestCase
{
    public function test_http_client_creation_failed_returns_instance_with_expected_message(): void
    {
        $actual = ApiException::httpClientInitializationFailed();
        $this->assertSame('HTTP client initialization failed.', $actual->getMessage());
    }

    public function test_http_client_initialization_failed_returns_instance_with_expected_code(): void
    {
        $actual = ApiException::httpClientInitializationFailed();
        $this->assertSame(ApiException::CODE_HTTP_CLIENT_INITIALIZATION_FAILED, $actual->getCode());
    }

    public function test_http_client_initialization_failed_returns_instance_with_expected_previous_exception(): void
    {
        $previous = HttpClientFactoryException::fromNotFoundException(new NotFoundException());
        $actual = ApiException::httpClientInitializationFailed($previous);
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_http_client_send_request_failed_returns_instance_with_expected_message(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubRequest = $this->createStub(RequestInterface::class);
        $previous = new NetworkException('Server unreachable', $stubRequest);
        $actual = ApiException::httpClientSendRequestFailed($previous);
        $this->assertSame('HTTP request failed.', $actual->getMessage());
    }

    public function test_http_client_send_request_failed_returns_instance_with_expected_code(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubRequest = $this->createStub(RequestInterface::class);
        $previous = new NetworkException('Server unreachable', $stubRequest);
        $actual = ApiException::httpClientSendRequestFailed($previous);
        $this->assertSame(ApiException::CODE_HTTP_CLIENT_SEND_REQUEST_FAILED, $actual->getCode());
    }

    public function test_http_client_send_request_failed_returns_instance_with_expected_previous_exception(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubRequest = $this->createStub(RequestInterface::class);
        $previous = new NetworkException('Server unreachable', $stubRequest);
        $actual = ApiException::httpClientSendRequestFailed($previous);
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_make_request_failed_returns_instance_with_expected_message(): void
    {
        $previous = new RequestFactoryException();
        $actual = ApiException::makeRequestFailed('create access token', $previous);
        $this->assertSame('Could not make create access token request.', $actual->getMessage());
    }

    public function test_make_request_failed_returns_instance_with_expected_code(): void
    {
        $previous = new RequestFactoryException();
        $actual = ApiException::makeRequestFailed('create access token', $previous);
        $this->assertSame(ApiException::CODE_MAKE_REQUEST_FAILED, $actual->getCode());
    }

    public function test_make_request_failed_returns_instance_with_expected_previous_exception(): void
    {
        $previous = new RequestFactoryException();
        $actual = ApiException::makeRequestFailed('create access token', $previous);
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_http_request_failed_returns_instance_with_expected_message(): void
    {
        $actual = ApiException::httpRequestFailed(401);
        $this->assertSame('Request failed with HTTP status code 401.', $actual->getMessage());
    }

    public function test_http_request_failed_returns_instance_with_expected_code(): void
    {
        $actual = ApiException::httpRequestFailed(401);
        $this->assertSame(401, $actual->getCode());
    }

    public function test_http_request_failed_returns_instance_with_null_previous_exception(): void
    {
        $actual = ApiException::httpRequestFailed(401);
        $this->assertNull($actual->getPrevious());
    }

    public function test_credential_creation_failed_returns_instance_with_expected_message(): void
    {
        $previous = new CredentialFactoryExceptionFake();
        $actual = ApiException::credentialCreationFailed($previous);
        $this->assertSame('Credential creation failed.', $actual->getMessage());
    }

    public function test_credential_creation_failed_returns_instance_with_expected_code(): void
    {
        $previous = new CredentialFactoryExceptionFake();
        $actual = ApiException::credentialCreationFailed($previous);
        $this->assertSame(ApiException::CODE_CREDENTIAL_CREATION_FAILED, $actual->getCode());
    }

    public function test_credential_creation_failed_returns_instance_with_expected_previous_exception(): void
    {
        $previous = new CredentialFactoryExceptionFake();
        $actual = ApiException::credentialCreationFailed($previous);
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_credential_storage_failed_returns_instance_with_expected_message(): void
    {
        $previous = new CredentialStoreExceptionFake();
        $actual = ApiException::credentialStorageFailed($previous);
        $this->assertSame('Credential storage failed.', $actual->getMessage());
    }

    public function test_credential_storage_failed_returns_instance_with_expected_code(): void
    {
        $previous = new CredentialStoreExceptionFake();
        $actual = ApiException::credentialStorageFailed($previous);
        $this->assertSame(ApiException::CODE_CREDENTIAL_STORAGE_FAILED, $actual->getCode());
    }

    public function test_credential_storage_failed_returns_instance_with_expected_previous_exception(): void
    {
        $previous = new CredentialStoreExceptionFake();
        $actual = ApiException::credentialStorageFailed($previous);
        $this->assertSame($previous, $actual->getPrevious());
    }
}
