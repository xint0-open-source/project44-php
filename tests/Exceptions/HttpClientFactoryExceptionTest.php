<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Exceptions;

use Http\Discovery\Exception\NotFoundException;
use PHPUnit\Framework\Attributes\CoversClass;
use Xint0\Project44\Exceptions\HttpClientFactoryException;
use Tests\TestCase;

#[CoversClass(HttpClientFactoryException::class)]
class HttpClientFactoryExceptionTest extends TestCase
{
    public function test_from_not_found_exception_returns_exception_with_expected_message(): void
    {
        $previousException = new NotFoundException("Previous exception.");
        $actual = HttpClientFactoryException::fromNotFoundException($previousException);
        $this->assertSame('HTTP client implementation not found.', $actual->getMessage());
    }

    public function test_from_not_found_exception_returns_exception_with_expected_code(): void
    {
        $previousException = new NotFoundException("Previous exception.");
        $actual = HttpClientFactoryException::fromNotFoundException($previousException);
        $this->assertSame(1, $actual->getCode());
    }

    public function test_from_not_found_exception_returns_exception_with_expected_previous_exception(): void
    {
        $previousException = new NotFoundException("Previous exception.");
        $actual = HttpClientFactoryException::fromNotFoundException($previousException);
        $this->assertSame($previousException, $actual->getPrevious());
    }
}
