<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Api;

use DateTimeImmutable;
use Exception;
use Http\Client\Exception\RequestException;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Concerns\UsesTestFixtures;
use Tests\Fakes\CredentialStoreExceptionFake;
use Tests\TestCase;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\MockCredentialStore\CredentialManager;
use Xint0\Project44\Api;
use Xint0\Project44\Enums\EventType;
use Xint0\Project44\Enums\ShipmentIdentifierType;
use Xint0\Project44\Exceptions\ApiException;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Factories\RequestFactory;
use Xint0\Project44\Models\ShipmentIdentifier;
use Xint0\Project44\Models\StatusUpdate;

#[CoversClass(Api::class)]
#[UsesClass(ApiException::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(RequestFactoryException::class)]
#[UsesClass(ShipmentIdentifier::class)]
#[UsesClass(StatusUpdate::class)]
class CreateStatusUpdateTest extends TestCase
{
    use UsesTestFixtures;

    /**
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function test_method_makes_expected_request(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $token = $fakeCredentialManager->getCredential('project44_token')->decryptSecret();
        $mockHttpClient = new Client();
        $requestMatcher = new RequestMatcher('/api/v4/capacityproviders/tl/shipments/statusUpdates', 'na12.api.project44.com', 'POST', 'https');
        $statusUpdate = (new StatusUpdate('CustomerId', new DateTimeImmutable('2023-02-02 17:15:01-07:00')))
            ->addShipmentIdentifier(new ShipmentIdentifier(ShipmentIdentifierType::ORDER, 'Order 1234'))
            ->setPosition(10.1234, 10.1234)
            ->setEventType(EventType::ARRIVED)
            ->setEventStopNumber(1);
        $mockHttpClient->on($requestMatcher, function (RequestInterface $request) use ($token, $statusUpdate) {
            $expectedAuthorizationHeader = ['Bearer ' . $token];
            $expectedBody = json_encode($statusUpdate);

            $this->assertSame($expectedAuthorizationHeader, $request->getHeader('Authorization'));
            $this->assertSame($expectedBody, $request->getBody()->getContents());

            $responseBodyStream = Psr17FactoryDiscovery::findStreamFactory()->createStream('test');
            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn(202);
            $stubResponse->method('getBody')
                ->willReturn($responseBodyStream);

            return $stubResponse;
        });
        $mockHttpClient->setDefaultException(new Exception('Unexpected request'));

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $sut->createStatusUpdate($statusUpdate);
    }

    public function test_method_throws_api_exception_when_credential_store_get_credential_fails(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubCredentialStore = $this->createStub(CredentialStoreInterface::class);
        $stubCredentialStore->method('getCredential')->willThrowException(new CredentialStoreExceptionFake('Credential not found'));
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultException(new Exception('Unexpected request'));
        $statusUpdate = new StatusUpdate('CustomerId');

        $sut = new Api($stubCredentialStore, $mockHttpClient);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Could not make create status update request.');
        $this->expectExceptionCode(ApiException::CODE_MAKE_REQUEST_FAILED);

        $sut->createStatusUpdate($statusUpdate);
    }

    public function test_method_throws_api_exception_when_http_client_send_request_fails(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = new RequestMatcher();
        $mockHttpClient->on($requestMatcher, function (RequestInterface $request) {
            throw new RequestException('Request failed', $request);
        });
        $statusUpdate = new StatusUpdate('CustomerId');

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $this->expectException(ApiException::class);
        $this->expectExceptionCode(ApiException::CODE_HTTP_CLIENT_SEND_REQUEST_FAILED);

        $sut->createStatusUpdate($statusUpdate);
    }

    public function test_method_throws_api_exception_when_response_http_status_is_bad_request(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = new RequestMatcher('/api/v4/capacityproviders/tl/shipments/statusUpdates', 'na12.api.project44.com', 'POST', 'https');
        $mockHttpClient->on($requestMatcher, function (RequestInterface $request) {
            $responsePath = $this->getFixtureFullPath('CreateStatusUpdate/400-bad_request.json');
            $bodyStream = Psr17FactoryDiscovery::findStreamFactory()->createStreamFromFile($responsePath);
            $responseFactory = Psr17FactoryDiscovery::findResponseFactory();
            return $responseFactory->createResponse(400)
                ->withBody($bodyStream)
                ->withHeader('Content-Type', 'application/json;charset=UTF-8');
        });

        $statusUpdate = new StatusUpdate('customerId');

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $this->expectException(ApiException::class);
        $this->expectExceptionCode(400);

        $sut->createStatusUpdate($statusUpdate);
    }

    public function test_method_throws_api_exception_when_response_http_status_code_is_unauthorized(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = new RequestMatcher('/api/v4/capacityproviders/tl/shipments/statusUpdates', 'na12.api.project44.com', 'POST', 'https');
        $mockHttpClient->on($requestMatcher, function (RequestInterface $request) {
            $responsePath = $this->getFixtureFullPath('CreateStatusUpdate/401-unauthorized.json');
            $bodyStream = Psr17FactoryDiscovery::findStreamFactory()->createStreamFromFile($responsePath);
            $responseFactory = Psr17FactoryDiscovery::findResponseFactory();
            return $responseFactory->createResponse(401)
                ->withBody($bodyStream)
                ->withHeader('Content-Type', 'application/json;charset=UTF-8');
        });

        $statusUpdate = new StatusUpdate('customerId');

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $this->expectException(ApiException::class);
        $this->expectExceptionCode(401);

        $sut->createStatusUpdate($statusUpdate);
    }
}
