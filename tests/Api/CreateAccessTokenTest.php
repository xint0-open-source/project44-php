<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Api;

use Exception;
use Http\Client\Exception\RequestException;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\RequestMatcher\RequestMatcher;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Tests\Concerns\UsesTestFixtures;
use Tests\Fakes\CredentialStoreExceptionFake;
use Tests\TestCase;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\MockCredentialStore\CredentialManager;
use Xint0\Project44\Api;
use Xint0\Project44\Exceptions\ApiException;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Factories\HttpClientFactory;
use Xint0\Project44\Factories\RequestFactory;

#[CoversClass(Api::class)]
#[UsesClass(ApiException::class)]
#[UsesClass(HttpClientFactory::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(RequestFactoryException::class)]
#[UsesClass(RequestFactoryInterface::class)]
class CreateAccessTokenTest extends TestCase
{
    use UsesTestFixtures;

    public function test_create_access_token_makes_expected_request(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = new RequestMatcher('/api/v4/oauth2/token', 'na12.api.project44.com', 'POST', 'https');
        $mockHttpClient->on($requestMatcher, function (RequestInterface $request) {
            $expectedAuthorizationHeader = ['Basic ' . base64_encode('client_id:client_secret')];
            $expectedBody = 'grant_type=client_credentials';

            $this->assertSame($expectedAuthorizationHeader, $request->getHeader('Authorization'));
            $this->assertSame($expectedBody, $request->getBody()->getContents());

            $stubResponse = $this->createStub(ResponseInterface::class);
            $stubResponse->method('getStatusCode')->willReturn(200);
            $stubResponse->method('getBody')
                ->willReturn($this->stubResponseBodyFromFixture('CreateAccessToken/success.json'));
            return $stubResponse;
        });
        $mockHttpClient->setDefaultException(new Exception('Unexpected request'));

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $sut->createAccessToken();
    }

    public function test_create_access_token_throws_api_exception_when_http_client_send_request_fails(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = new RequestMatcher();
        $mockHttpClient->on($requestMatcher, function (RequestInterface $request) {
            throw new RequestException('Request failed', $request);
        });

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $this->expectException(ApiException::class);

        $sut->createAccessToken();
    }

    public function test_create_access_token_throws_api_exception_when_credential_store_get_credential_fails(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubCredentialStore = $this->createStub(CredentialStoreInterface::class);
        $stubCredentialStore->method('getCredential')->willThrowException(new CredentialStoreExceptionFake('Credential not found'));
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultException(new Exception('Unexpected request'));

        $sut = new Api($stubCredentialStore, $mockHttpClient);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Could not make create access token request.');
        $this->expectExceptionCode(ApiException::CODE_MAKE_REQUEST_FAILED);

        $sut->createAccessToken();
    }

    public function test_create_access_token_throws_api_exception_when_response_status_code_is_not_success(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = $this->createAccessTokenRequestMatcher();
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubResponse = $this->createStub(ResponseInterface::class);
        $stubResponse->method('getStatusCode')->willReturn(401);
        $stubResponse->method('getBody')->willReturn($this->stubResponseBodyFromFixture('CreateAccessToken/unauthorized.json'));
        $mockHttpClient->on($requestMatcher, $stubResponse);

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        $this->expectException(ApiException::class);
        $this->expectExceptionCode(401);
        $this->expectExceptionMessage('Request failed with HTTP status code 401.');

        $sut->createAccessToken();
    }

    public function test_create_access_token_returns_response_with_updated_credential_on_success(): void
    {
        /** @var object{access_token:string,token_type:string,expires_in:int} $successResponseData */
        $successResponseData = json_decode($this->getFixtureContents('CreateAccessToken/success.json'));
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $mockHttpClient = new Client();
        $requestMatcher = $this->createAccessTokenRequestMatcher();
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubResponse = $this->createStub(ResponseInterface::class);
        $stubResponse->method('getStatusCode')->willReturn(200);
        $stubResponse->method('getBody')
            ->willReturn($this->stubResponseBodyFromFixture('CreateAccessToken/success.json'));
        $mockHttpClient->on($requestMatcher, $stubResponse);

        $sut = new Api($fakeCredentialManager, $mockHttpClient);

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->assertNotSame($successResponseData->access_token, $fakeCredentialManager->getCredential('project44_token')->decryptSecret());

        $actual = $sut->createAccessToken();

        $this->assertSame($stubResponse, $actual);
    }

    private function stubResponseBodyFromFixture(string $fixturePath): StreamInterface
    {
        $streamFactory = Psr17FactoryDiscovery::findStreamFactory();
        return $streamFactory->createStream($this->getFixtureContents($fixturePath));
    }

    private function createAccessTokenRequestMatcher(): RequestMatcher
    {
        return new RequestMatcher('/api/v4/oauth2/token', 'na12.api.project44.com', 'POST', 'https');
    }
}
