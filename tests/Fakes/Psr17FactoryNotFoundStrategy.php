<?php

namespace Tests\Fakes;

use Http\Discovery\Exception\DiscoveryFailedException;
use Http\Discovery\Strategy\DiscoveryStrategy;
use Psr\Http\Message\RequestFactoryInterface;

class Psr17FactoryNotFoundStrategy implements DiscoveryStrategy
{
    public static string $throw_for_type = RequestFactoryInterface::class;

    /**
     * @inheritDoc
     * @throws DiscoveryFailedException
     * @return array<int,array{class: string, condition: mixed}>
     */
    public static function getCandidates($type): array
    {
        if ($type === self::$throw_for_type) {
            throw new DiscoveryFailedException("$type not found");
        }

        return [];
    }
}
