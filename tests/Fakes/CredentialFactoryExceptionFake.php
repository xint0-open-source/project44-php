<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Fakes;

use RuntimeException;
use Xint0\CredentialStorage\Contracts\CredentialFactoryExceptionInterface;

class CredentialFactoryExceptionFake extends RuntimeException implements CredentialFactoryExceptionInterface
{
}
