<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests;

use Http\Discovery\Psr18ClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\Concerns\ResetsHttpClientDiscoveryStrategies;
use Xint0\MockCredentialStore\CredentialManager;
use Xint0\Project44\Api;
use Xint0\Project44\Exceptions\ApiException;
use Xint0\Project44\Exceptions\HttpClientFactoryException;
use Xint0\Project44\Factories\HttpClientFactory;
use Xint0\Project44\Factories\RequestFactory;

#[CoversClass(Api::class)]
#[UsesClass(ApiException::class)]
#[UsesClass(HttpClientFactory::class)]
#[UsesClass(HttpClientFactoryException::class)]
#[UsesClass(RequestFactory::class)]
class ApiTest extends TestCase
{
    use ResetsHttpClientDiscoveryStrategies;

    public function test_creates_default_http_client_when_not_specified(): void
    {
        $credentialStore = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        Psr18ClientDiscovery::prependStrategy(MockClientStrategy::class);
        $sut = new Api($credentialStore);
        $this->assertInstanceOf(Client::class, $sut->httpClient());
    }

    public function test_constructor_throws_exception_when_default_http_client_cannot_be_created(): void
    {
        $credentialStore = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        // Ensure HttpClientDiscovery throws exception by removing MockClientStrategy.
        $this->resetHttpClientDiscoveryStrategies();
        $this->expectException(ApiException::class);
        new Api($credentialStore);
    }
}
