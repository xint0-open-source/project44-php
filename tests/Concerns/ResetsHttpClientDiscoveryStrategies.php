<?php

namespace Tests\Concerns;

use Http\Discovery\Psr18ClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use Tests\TestCase;

/**
 * @mixin TestCase
 */
trait ResetsHttpClientDiscoveryStrategies
{
    /**
     * Remove MockClientStrategy from HttpClientDiscovery strategies.
     *
     * @return void
     */
    public function resetHttpClientDiscoveryStrategies(): void
    {
        $strategies = array_filter(
            self::currentStrategies(),
            fn ($s) => $s !== MockClientStrategy::class
        );
        Psr18ClientDiscovery::setStrategies($strategies);
    }

    /**
     * @return string[]
     */
    private static function currentStrategies(): array
    {
        $strategies = [];
        foreach (Psr18ClientDiscovery::getStrategies() as $strategy) {
            $strategies[] = $strategy;
        }
        return $strategies;
    }
}
