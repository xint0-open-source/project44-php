<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Factories;

use Http\Discovery\Psr18ClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\TestCase;
use Xint0\Project44\Exceptions\HttpClientFactoryException;
use Xint0\Project44\Factories\HttpClientFactory;

#[CoversClass(HttpClientFactory::class)]
#[UsesClass(HttpClientFactoryException::class)]
class HttpClientFactoryTest extends TestCase
{
    public function test_make_returns_psr_http_client_interface(): void
    {
        Psr18ClientDiscovery::prependStrategy(MockClientStrategy::class);
        $sut = new HttpClientFactory();
        $this->assertInstanceOf(Client::class, $sut->make());
    }

    public function test_throws_http_client_factory_exception_when_client_cannot_be_found(): void
    {
        $strategies = self::currentStrategies();
        if (in_array(MockClientStrategy::class, $strategies)) {
            $strategies = array_filter($strategies, fn ($s) => $s !== MockClientStrategy::class);
            Psr18ClientDiscovery::setStrategies($strategies);
        }
        Psr18ClientDiscovery::clearCache();
        $this->expectException(HttpClientFactoryException::class);
        $sut = new HttpClientFactory();
        $sut->make();
    }

    /**
     * @return string[]
     */
    private static function currentStrategies(): array
    {
        $strategies = [];
        foreach (Psr18ClientDiscovery::getStrategies() as $strategy) {
            $strategies[] = $strategy;
        }
        return $strategies;
    }
}
