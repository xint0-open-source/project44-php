<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Factories\RequestFactory;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\Fakes\CredentialStoreExceptionFake;
use Xint0\CredentialStorage\Contracts\CredentialInterface;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Factories\RequestFactory;
use Tests\TestCase;

#[CoversClass(RequestFactory::class)]
#[UsesClass(RequestFactoryException::class)]
class MakeCreateAccessTokenRequestTest extends TestCase
{
    public function test_make_create_access_token_request_returns_request_with_expected_properties(): void
    {
        $stubClientCredential = $this->stubClientCredential('client_identity', 'client_secret');
        $stubCredentialStore = $this->stubCredentialStore([['project44_client', $stubClientCredential]]);
        $expectedAuthorizationHeader = 'Basic ' . base64_encode('client_identity:client_secret');
        $expectedBody = 'grant_type=' . urlencode('client_credentials');

        $sut = new RequestFactory($stubCredentialStore);
        $request = $sut->makeCreateAccessTokenRequest();

        $this->assertSame('https', $request->getUri()->getScheme());
        $this->assertSame('na12.api.project44.com', $request->getUri()->getHost());
        $this->assertSame('/api/v4/oauth2/token', $request->getUri()->getPath());
        $this->assertSame(['application/x-www-form-urlencoded'], $request->getHeader('Content-Type'));
        $this->assertSame(['application/json'], $request->getHeader('Accept'));
        $this->assertSame([$expectedAuthorizationHeader], $request->getHeader('Authorization'));
        $this->assertSame($expectedBody, $request->getBody()->getContents());
    }

    public function test_throws_request_factory_exception_when_client_credential_cannot_be_retrieved_from_credential_store(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubCredentialStore = $this->createStub(CredentialStoreInterface::class);
        $stubException = new CredentialStoreExceptionFake('Credential not found');
        $stubCredentialStore->method('getCredential')->willThrowException($stubException);
        $this->expectException(RequestFactoryException::class);

        $sut = new RequestFactory($stubCredentialStore);
        $sut->makeCreateAccessTokenRequest();
    }

    private function stubClientCredential(string $identity, string $secret): CredentialInterface
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $clientCredentialStub = $this->createStub(CredentialInterface::class);
        $clientCredentialStub->method('getIdentity')->willReturn($identity);
        $clientCredentialStub->method('decryptSecret')->willReturn($secret);
        return $clientCredentialStub;
    }

    /**
     * @param  array<int,array<int,mixed>>  $credentialMap
     *
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDocMissingThrowsInspection
     */
    private function stubCredentialStore(array $credentialMap): CredentialStoreInterface
    {
        $stub = $this->createStub(CredentialStoreInterface::class);
        $stub->method('getCredential')
            ->willReturnMap($credentialMap);
        return $stub;
    }
}
