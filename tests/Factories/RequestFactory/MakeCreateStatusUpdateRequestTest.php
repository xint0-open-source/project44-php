<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Factories\RequestFactory;

use DateTimeImmutable;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Tests\Fakes\CredentialStoreExceptionFake;
use Tests\TestCase;
use Xint0\CredentialStorage\Contracts\CredentialStoreInterface;
use Xint0\MockCredentialStore\CredentialManager;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Factories\RequestFactory;
use Xint0\Project44\Models\StatusUpdate;

#[CoversClass(RequestFactory::class)]
#[UsesClass(RequestFactoryException::class)]
#[UsesClass(StatusUpdate::class)]
class MakeCreateStatusUpdateRequestTest extends TestCase
{
    public function test_returns_request_interface_with_expected_values(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        /** @noinspection PhpUnhandledExceptionInspection */
        $expectedAuthorizationHeader = 'Bearer ' . $fakeCredentialManager
                ->getCredential('project44_token')
                ->decryptSecret();
        if (false === $timestamp = DateTimeImmutable::createFromFormat(DATE_ATOM, '2023-02-02T13:27:12-07:00')) {
            self::fail('Could not create timestamp.');
        }
        $statusUpdate = new StatusUpdate('CustomerId', $timestamp);
        $expectedBody = json_encode($statusUpdate);

        $sut = new RequestFactory($fakeCredentialManager);
        $request = $sut->makeCreateStatusUpdateRequest($statusUpdate);

        $this->assertSame('https', $request->getUri()->getScheme());
        $this->assertSame('na12.api.project44.com', $request->getUri()->getHost());
        $this->assertSame('/api/v4/capacityproviders/tl/shipments/statusUpdates', $request->getUri()->getPath());
        $this->assertSame(['application/json'], $request->getHeader('Content-Type'));
        $this->assertSame(['application/json'], $request->getHeader('Accept'));
        $this->assertSame([$expectedAuthorizationHeader], $request->getHeader('Authorization'));
        $this->assertSame($expectedBody, $request->getBody()->getContents());
    }

    public function test_throws_request_factory_exception_when_token_credential_cannot_be_retrieved_from_credential_store(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stubCredentialStore = $this->createStub(CredentialStoreInterface::class);
        $stubException = new CredentialStoreExceptionFake('Credential not found');
        $stubCredentialStore->method('getCredential')->willThrowException($stubException);
        $statusUpdate = new StatusUpdate('CustomerId');

        $this->expectException(RequestFactoryException::class);

        $sut = new RequestFactory($stubCredentialStore);
        $sut->makeCreateStatusUpdateRequest($statusUpdate);
    }
}
