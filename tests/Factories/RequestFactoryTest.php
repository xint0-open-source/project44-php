<?php

/**
 * xint0/project44-php
 *
 * Project44 API client.
 *
 * @author Rogelio Jacinto Pascual
 * @copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/project44-php/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Factories;

use Http\Discovery\Psr17FactoryDiscovery;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\UsesClass;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Tests\Fakes\Psr17FactoryNotFoundStrategy;
use Xint0\MockCredentialStore\CredentialManager;
use Xint0\Project44\Exceptions\RequestFactoryException;
use Xint0\Project44\Factories\RequestFactory;
use PHPUnit\Framework\TestCase;

#[CoversClass(RequestFactory::class)]
#[UsesClass(RequestFactoryException::class)]
class RequestFactoryTest extends TestCase
{
    public function test_constructor_throws_request_factory_exception_when_request_factory_is_not_found(): void
    {
        Psr17FactoryNotFoundStrategy::$throw_for_type = RequestFactoryInterface::class;
        Psr17FactoryDiscovery::prependStrategy(Psr17FactoryNotFoundStrategy::class);
        $this->expectException(RequestFactoryException::class);
        $this->expectExceptionMessage('Request factory not found');
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        new RequestFactory($fakeCredentialManager);
    }

    public function test_constructor_throws_request_factory_exception_when_uri_factory_is_not_found(): void
    {
        Psr17FactoryNotFoundStrategy::$throw_for_type = UriFactoryInterface::class;
        Psr17FactoryDiscovery::prependStrategy(Psr17FactoryNotFoundStrategy::class);
        $this->expectException(RequestFactoryException::class);
        $this->expectExceptionMessage('URI factory not found');
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        new RequestFactory($fakeCredentialManager);
    }

    public function test_constructor_throws_request_factory_exception_when_stream_factory_is_not_found(): void
    {
        Psr17FactoryNotFoundStrategy::$throw_for_type = StreamFactoryInterface::class;
        Psr17FactoryDiscovery::prependStrategy(Psr17FactoryNotFoundStrategy::class);
        $this->expectException(RequestFactoryException::class);
        $this->expectExceptionMessage('Stream factory not found');
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        new RequestFactory($fakeCredentialManager);
    }

    public function test_instance_initialized_with_specified_credential_store(): void
    {
        $fakeCredentialManager = new CredentialManager([
            'project44_user' => ['identity' => 'username', 'secret' => 'password'],
            'project44_client' => ['identity' => 'client_id', 'secret' => 'client_secret'],
            'project44_token' => ['identity' => 'token_id', 'secret' => 'token'],
        ]);
        $actual = new RequestFactory($fakeCredentialManager);
        $this->assertSame($fakeCredentialManager, $actual->credentialStore());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $remove_strategies = [
            Psr17FactoryNotFoundStrategy::class,
        ];
        $strategies = array_filter(self::currentStrategies(), fn ($s) => ! in_array($s, $remove_strategies));
        Psr17FactoryDiscovery::setStrategies($strategies);
    }

    /**
     * @return string[]
     */
    private static function currentStrategies(): array
    {
        $strategies = [];
        foreach (Psr17FactoryDiscovery::getStrategies() as $strategy) {
            $strategies[] = $strategy;
        }
        return $strategies;
    }
}
